(function (Drupal) {
  'use strict';

  Drupal.behaviors.unishare = {
    attach: function (context, settings) {
      var buttons = context.querySelectorAll('[data-unishare-button]');

      var listener;
      if (navigator.share) {
        listener = function (event) {
          var url = event.target.getAttribute('data-url');
          var title = event.target.getAttribute('data-title');
          var description = event.target.getAttribute('data-description');
          navigator.share({
            title: title,
            text: description,
            url: url,
          });
        };
      } else {
        listener = function (event) {
          var target = event.target;
          var container = target.closest('[data-unishare]');
          var dialogElement = container.querySelector('[data-unishare-dialog]');
          var dialog = Drupal.dialog(dialogElement.cloneNode(true), {title: Drupal.t('Share', {}, {context: 'unishare'})});
          dialog.showModal();
        };
      }

      buttons.forEach((button) => {
        button.classList.remove('visually-hidden');
        button.addEventListener('click', listener);
      });
    }
  }

})(Drupal);
