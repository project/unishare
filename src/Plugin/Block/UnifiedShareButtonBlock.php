<?php

namespace Drupal\unishare\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block with a unified share button.
 *
 * @Block(
 *   id = "unishare",
 *   admin_label = @Translation("Unified Share Button"),
 * )
 */
class UnifiedShareButtonBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * @var \Drupal\Core\Controller\TitleResolverInterface
   */
  protected $titleResolver;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
    $plugin->renderer = $container->get('renderer');
    $plugin->currentRouteMatch = $container->get('current_route_match');
    $plugin->titleResolver = $container->get('title_resolver');
    $plugin->requestStack = $container->get('request_stack');
    $plugin->configFactory = $container->get('config.factory');
    return $plugin;
  }

  /**
   * @inheritDoc
   */
  public function build() {
    $icon = base_path() . \Drupal::service('extension.list.module')->getPath('unishare') . '/images/share.svg';
    $class = ['unishare-button', 'unishare-fixed-icon', 'visually-hidden'];
    $url = $this->requestStack->getCurrentRequest()->getUri();
    $route = $this->currentRouteMatch->getRouteObject();
    $title = $this->titleResolver->getTitle($this->requestStack->getCurrentRequest(), $route);
    $description = $this->configFactory->get('system.site')->get('name');
    $build['unishare'] = [
      '#theme' => 'unishare_button',
      '#icon' => $icon,
      '#url' => $url,
      '#title' => $title,
      '#description' => $description,
      '#classes' => $class,
      '#attached' => [
        'library' => [
          'unishare/button'
        ],
      ],
      '#cache' => [
        'contexts' => ['url.path'],
      ],
    ];
    $this->renderer->addCacheableDependency($build, $route);
    return $build;
  }
}
